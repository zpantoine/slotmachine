# README #

SlotMachine app written in AS3 with the Starling 2.1 library.

[You can see it in action here](https://zpantoine.bitbucket.io/)

# Update #
* Created a web app to configure the slot machine. You can [find it here](https://desolate-garden-94254.herokuapp.com/).

# Notes #

* I designed the app to be driven by a config file ([src/data/config.json](https://bitbucket.org/zpantoine/slotmachine/src/209c4fbef3c9963969c9a14ec6772d1fea4e8f1e/src/data/config.json?at=master&fileviewer=file-view-default))
* When I was starting out, I was planning on making a webapp to setup the and deploy the config (and I still might at some point)
* I created all the art work from scratch in Photoshop. It's beautiful I know.

# In the config #
* You can specify the number of rows and columns for the playfield.
* The payline type can be "single" or "any". If "single", the symbols must match across a single row. "rows" is an array that specifies which will be checked for winning combination. If the type is "any", the game will look for diagonal neighbors that match as well.
* The symbols parameter specifies the spritesheet url and a corresponding texture atlas. My goal was to accept images to stitch together in the configuration web app.
* Symbols also has a "count" parameter that specifies the number of symbols that will be used in the game. Please note that 10 is the max here.