package
{
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	
	import starling.textures.Texture;
	
	/**
	 * Resource class to store and retrieve texture assets
	 */
	public final class R
	{
		public static const BACKGROUND_TX:String = "background";
		public static const SCREEN_OVERLAY_TX:String = "screenOverlay";
		public static const LOADING_SCREEN_TX:String = "loadingScreen";
		public static const SPIN_BTN_UP_TX:String = "spinUp";
		public static const SPIN_BTN_DOWN_TX:String = "spinDown";
		public static const SPIN_BTN_DIS_TX:String = "spinDisabled";
		public static const SPRITE_SHEET_TX:String = "spriteSheet";
		public static const WAGER_BTNS_TX:String = "wagerBtns";
		
		[Embed(source="assets/background.png")]
		public static const background:Class;
		
		[Embed(source="assets/screen_overlay.png")]
		public static const screenOverlay:Class;
		
		[Embed(source="assets/loading.png")]
		public static const loadingScreen:Class;
		
		[Embed(source="assets/spin_up.png")]
		public static const spinUp:Class;
		
		[Embed(source="assets/spin_down.png")]
		public static const spinDown:Class;
		
		[Embed(source="assets/spin_disabled.png")]
		public static const spinDisabled:Class;
		
		[Embed(source="assets/wagers_btns.png")]
		public static const wagerBtns:Class;
		
		private static var gameTextures:Dictionary = new Dictionary();
		
		/**
		 * Returns a texture given a class name. Saves into the game textures dictionary on first call
		 */
		public static function getTexture(name:String):Texture
		{
			if(gameTextures[name] == undefined && R[name]) {
				var bmp:Bitmap = new R[name]();
				gameTextures[name] = Texture.fromBitmap(bmp);
			}
			
			return gameTextures[name];
		}
		
		/**
		 * Saves externally loaded images into the game textures dictionary
		 */
		public static function saveTexture(name:String, bitmap:Bitmap):void
		{
			gameTextures[name] = Texture.fromBitmap(bitmap);
		}
	}
}