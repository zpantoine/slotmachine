package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	
	import starling.core.Starling;
	
	[SWF(frameRate="60", width="1280", height="752", backgroundColor="0xcccccc")]
	public class SlotMachine extends Sprite
	{
		private var _starling:Starling;
		
		public function SlotMachine()
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			_starling = new Starling(GameController, stage);
			_starling.start();
		}
	}
}