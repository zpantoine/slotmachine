package com.za.model
{
	import starling.events.EventDispatcher;
	
	/**
	 * Singleton Model containing all game data
	 */
	public class ModelLocator extends EventDispatcher
	{
		private static var _instance:ModelLocator;
		
		private var _config:Object;
		public var controller:GameController;
		
		public var paylineType:String;	// "single" or "any"
		public var paylineRows:Array;	// array of the rows on which a payline is allowed
		
		public var spritesheetURL:String;
		public var symbolInfo:Array;
		public var numSymbols:int;
		
		public var startBalance:int;
		public var wager:int;
		public var player:Player;
		public var playfield:Playfield;
		
		public function ModelLocator(enforcer:SingletonEnforcer) {	}
		
		public static function get instance():ModelLocator {
			
			if(!_instance) {
				_instance = new ModelLocator(new SingletonEnforcer());
			}
			return _instance;
		}
		
		public function get config():Object {
			return _config;
		}
		
		public function set config(value:Object):void {
			_config = value;
			parseConfig(value);
			
			controller.loadExternalImages();
		}
		
		private function parseConfig(config:Object):void {
			
			// initialize payline data
			paylineType = config.game.payline.type;
			paylineRows = config.game.payline.rows;
			
			// initialize symbols
			spritesheetURL = config.game.symbols.spritesheet_url;
			symbolInfo = config.game.symbols.info;
			numSymbols = config.game.symbols.count;
			
			// Initialize playfield data
			playfield = new Playfield(config.game.playfield.rows, config.game.playfield.columns);
			
			// init player
			startBalance = config.game.player.startBalance;
			wager = config.game.player.wager;
			player = new Player();
		}
	}
}class SingletonEnforcer {} // private class to insure Model is only instantiated once