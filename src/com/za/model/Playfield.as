package com.za.model
{
	public class Playfield
	{
		private var _rows:int;
		private var _cols:int;
		
		private var _model:ModelLocator;
		private var _reels:Vector.<Reel>;
		
		private var _winningLines:Array;
		
		/**
		 * Represents the collection of reels
		 */
		public function Playfield(rows:int, columns:int)
		{
			_model = ModelLocator.instance;
			_reels = new Vector.<Reel>();
			_rows = rows;
			_cols = columns;
			
			for(var i:uint;i < columns;++i) {
				_reels.push(new Reel(_model.numSymbols));
			}
		}
		
		/**
		 * Preserve the order of the reel, but find a new top-most symbol
		 */
		public function spinReels():void {
			for each(var reel:Reel in reels) {
				reel.randomizePosition();
			}
		}
		
		/**
		 * Check the reels for a winning situation (stored in winningLines)
		 */
		public function checkPaylines():void
		{
			_winningLines = [];
			
			for(var r:int=0;r < rows;++r) {
				var payline:Array = [r];
				var currentCol:int = 0;
				
				var nextRow:int = checkNeighbor(r, currentCol, _model.paylineType == "single");
				while(nextRow > -1)
				{
					payline.push(nextRow);
					currentCol++;
					nextRow = checkNeighbor(nextRow, currentCol, _model.paylineType == "single");
				}
				
				if(payline.length == cols) {
					
					if(_model.paylineType == "any" || (_model.paylineType == "single" && _model.paylineRows.indexOf(payline[0]) > -1))
						_winningLines.push(payline);
				}
			}
		}
		
		public function get winningLines():Array {
			return _winningLines;
		}
		
		/**
		 * Checks the neighboring reel for a symbol match.
		 * setting limitSameRow to true will prevent diagonal matches
		 */
		private function checkNeighbor(r:int, c:int, limitSameRow:Boolean = false):int
		{
			if(c+1 >= cols) // final reel in the playfield
				return -1;
			
			var symbol1:int = reels[c].getSymbolAt(r);
			var nextReel:Reel = reels[c+1];
			var symbol2:int;
			
			if(limitSameRow) {
				symbol2 = nextReel.getSymbolAt(r);
				if(symbol2 == symbol1)
					return r;
			}
			else {
				var prevRow:int = (r - 1) < 0 ? 0 : r - 1;
				var nextRow:int = (r + 1) >= rows ? rows - 1 : r + 1;
				
				for(var k:int=prevRow;k <= nextRow;++k) {
					
					symbol2 = nextReel.getSymbolAt(k);
					if(symbol1 == symbol2)
						return k;
				}
			}
			
			return -1;
		}
		
		public function get reels():Vector.<Reel> {
			return _reels;
		}
		
		public function get rows():int {
			return _rows;
		}
		
		public function get cols():int {
			return _cols;
		}
	}
}