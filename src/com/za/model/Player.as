package com.za.model
{
	public class Player
	{
		private var _model:ModelLocator;
		
		private var _balance:int;
		private var _wager:int;
		private var _wagerInc:int; // the amount a player can increment the wager
		
		public function Player()
		{
			_model = ModelLocator.instance;
			reset();
		}
		
		public function reset():void
		{
			_balance = _model.startBalance;
			_wager = _wagerInc = _model.wager;
			
			_model.controller.handlePlayerUpdate();
		}
		
		/**
		 * Multiplier is a percent written as a whole number (10, 20, etc).
		 */
		public function addWinnings(multiplier:int):void
		{
			var percentIncrease:Number = (100 + multiplier) * 0.01;
			balance += wager * percentIncrease;
			
			wager = 0; // will set to minimum wager
		}
		
		public function deductWager():void
		{
			balance -= _wager;
			wager = 0; // will set to the minimum wager
		}
		
		public function increaseWager():void 
		{
			wager += _wagerInc;
		}
		
		public function decreaseWager():void
		{
			wager -= _wagerInc;
		}
		
		public function get balance():int {
			return _balance;
		}
		
		public function set balance(value:int):void {
			_balance = (value < 0) ? 0 : value;
			_model.controller.handlePlayerUpdate();
		}
		
		public function get wager():int {
			return _wager;
		}
		
		public function set wager(value:int):void {
			
			// don't allow the player to wager more than they have in funds
			_wager = (value > _balance) ? balance : value;
			
			// if the player has less money than the minimum wager in their balance, allow them to wager everything the have left
			if(_wager < _wagerInc) {
				_wager = (_balance < _wagerInc) ? _balance : _wagerInc;
			}
			
			_model.controller.handlePlayerUpdate();
		}
	}
}