package com.za.model
{
	public class Reel
	{
		private var _symbols:Array;
		
		// the element at the "top" of the reel
		private var _position:int;
		
		public function Reel(numSymbols:int)
		{
			_symbols = [];
			_position = 0;
			
			shuffleSymbols(numSymbols);
			randomizePosition();
		}
		
		private function shuffleSymbols(count:int):void
		{
			var seed:Array = [];
			for(var i:uint=0;i < count;++i)
				seed.push(i);
			
			var ind:int;
			while(seed.length > 0) {
				ind = Math.round(Math.random() * (seed.length - 1));
				_symbols.push(seed.splice(ind, 1));
			}
		}
		
		/**
		 * Returns the element offset a certain amount from the current position
		 */
		public function getSymbolAt(offset:int):int {
			var index:int = (_position + offset) % _symbols.length;
			return _symbols[index];
		}
		
		public function randomizePosition():void 
		{
			_position = Math.round(Math.random() * (_symbols.length - 1));
		}
		
		public function get position():int {
			return _position;
		}
		
		public function set position(value:int):void {
			_position = value;
		}
		
		public function toString():String {
			return "[Reel " + _symbols.toString() +"]";
		}
	}
}