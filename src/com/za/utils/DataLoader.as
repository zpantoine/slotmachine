package com.za.utils
{
	import com.za.events.SlotEvent;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import starling.events.EventDispatcher;

	public class DataLoader extends EventDispatcher
	{
		public static const TYPE_JSON:String = "json";
		public static const TYPE_IMAGE:String = "image";
		
		private var _data:*;
		
		public function DataLoader()
		{
		}
		
		public function load(url:String, type:String):void
		{
			switch(type) {
				case TYPE_JSON:
					
					var loader:URLLoader = new URLLoader();
					loader.addEventListener(Event.COMPLETE, onLoadComplete);
					loader.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);
					loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoadError);
					loader.load(new URLRequest(url));
					break;
				case TYPE_IMAGE:
					var imgLoader:Loader = new Loader();
					imgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoadComplete);
					imgLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onImageLoadError);
					imgLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onImageLoadError);
					imgLoader.load(new URLRequest(url));
					break;
			}
		}
		
		private function onLoadComplete(e:Event):void
		{
			var loader:URLLoader = e.target as URLLoader;
			dispatchEvent(new SlotEvent(SlotEvent.LOAD_COMPLETE, false, loader.data));
		}
		
		private function onLoadError(e:*):void
		{
			dispatchEvent(new SlotEvent(SlotEvent.LOAD_ERROR));
		}
		
		private function onImageLoadComplete(e:Event):void 
		{
			var loaderInfo:LoaderInfo = e.target as LoaderInfo;
			dispatchEvent(new SlotEvent(SlotEvent.LOAD_COMPLETE, false, loaderInfo.content));
		}
		
		private function onImageLoadError(e:*):void 
		{
			dispatchEvent(new SlotEvent(SlotEvent.LOAD_ERROR));
		}
	}
}