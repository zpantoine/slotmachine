package com.za.view
{
	import com.za.events.SlotEvent;
	import com.za.model.ModelLocator;
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;

	/**
	 * Graphics draw on the playfield to show users the winning lines
	 */
	public class PaylineView extends starling.display.Sprite
	{
		private var _model:ModelLocator;
		
		private var _graphicsSprite:flash.display.Sprite;
		private var _bdata:BitmapData;
		
		private var _displayHeight:int;
		private var _displayWidth:int;
		
		private var _image:Image;
		private var _circles:Vector.<Point>
		
		public function PaylineView(width:int, height:int)
		{
			_model = ModelLocator.instance;
			
			_displayWidth = width;
			_displayHeight = height;
			_bdata = new BitmapData(_displayWidth, _displayHeight, true, 0x00000000);
			
			_graphicsSprite = new flash.display.Sprite();
			_bdata.draw(_graphicsSprite);
			
			_image = new Image(Texture.fromBitmapData(_bdata));
			addChild(_image);
			
			_circles = new Vector.<Point>();
		}
		
		public function clear():void
		{
			_graphicsSprite.graphics.clear();
			_bdata = new BitmapData(_displayWidth, _displayHeight, true, 0x00000000);
			_image.texture = Texture.fromBitmapData(_bdata);
		}
		
		public function addCircle(x:int, y:int, radius:int, line:int, column:int):void
		{					
			var delay:int = column + line * _model.playfield.cols; // delay uses line (row) and colum to animate one line at a time, one column at a time
			
			var circleTween:Tween = new Tween(this, delay);
			circleTween.onComplete = drawCircle;
			circleTween.onCompleteArgs = [x, y, radius, column];
			Starling.juggler.add(circleTween);
		}
		
		private function drawCircle(x:Number, y:Number, radius:int, column:int):void 
		{	
			// if there is a previous circle on this line, draw a line to it
			if(_circles.length > 0 && column > 0) {
				var previous:Point = _circles.pop();
				_graphicsSprite.graphics.lineStyle(5, 0);
				_graphicsSprite.graphics.moveTo(previous.x, previous.y);
				_graphicsSprite.graphics.lineTo(x, y);
			}
			else {
				_circles.pop();
				_graphicsSprite.graphics.moveTo(x, y);
			}
			
			_circles.push(new Point(x,y));			
			_graphicsSprite.graphics.lineStyle(10, 0);
			_graphicsSprite.graphics.drawCircle(x, y, radius);
			_bdata.draw(_graphicsSprite);
			_image.texture = Texture.fromBitmapData(_bdata);
		
			// dispatch an event to keep track of how many lines have been drawm so the game can continue
			if(column == _model.playfield.cols - 1)
				dispatchEvent(new SlotEvent(SlotEvent.PAYLINE_DRAWN));
		}
	}
}