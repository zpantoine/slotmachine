package com.za.view
{
	import com.za.events.SlotEvent;
	import com.za.model.ModelLocator;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	/**
	 * Represents the playfield graphics
	 */
	public class PlayfieldView extends Sprite
	{
		private var _model:ModelLocator;
		private var _displayWidth:int;
		private var _displayHeight:int;
		private var _symbolWidth:int;
		private var _symbolHeight:int;
		private var _reels:Vector.<ReelView>;
		
		private var _paylineView:PaylineView;
		private var _linesDrawn:int;
		
		public function PlayfieldView(width:int, height:int)
		{
			_model = ModelLocator.instance;
			_displayWidth = width;
			_displayHeight = height;
			
			mask = new Image(Texture.fromColor(_displayWidth, _displayHeight, 0));
			
			_reels = new Vector.<ReelView>();
		}
		
		/**
		 * Popuplate the reels with symbols and add to the display
		 */
		public function createReels():void
		{
			_symbolWidth = Math.round(_displayWidth / _model.playfield.cols);
			_symbolHeight = Math.round(_displayHeight / _model.playfield.rows);
			
			for(var i:uint=0;i < _model.playfield.reels.length;++i) {
				_reels[i] = new ReelView(i, _symbolWidth, _symbolHeight);
				addChild(_reels[i]);				
			}
			
			centerReels();
		}
		
		public function animateReels():void
		{
			if(_paylineView)
				_paylineView.clear();
			
			for each(var reel:ReelView in _reels) {
				reel.animateSpin();
			}
		}
		
		private function centerReels():void
		{
			var xgap:int = (_displayWidth - _reels[0].width * _reels.length) / (_reels.length + 1);
			for(var i:uint=0;i < _reels.length;++i) {
				_reels[i].x = (1 + i) * xgap + _reels[i].width * i;
			}
		}
		
		public function drawPaylines():void
		{
			if(!_paylineView) {
				_paylineView = new PaylineView(_displayWidth, _displayHeight);
				_paylineView.addEventListener(SlotEvent.PAYLINE_DRAWN, onLineDrawn);
				addChild(_paylineView);
			}
			
			_linesDrawn = 0;
			
			var line:Array;
			for(var i:uint=0;i < _model.playfield.winningLines.length;++i) {
				
				line = _model.playfield.winningLines[i];
				
				for(var j:uint=0;j < line.length;++j) {
					var x:int = _reels[j].x + _reels[j].width * 0.5;
					var y:int = _symbolHeight * line[j] + _symbolHeight * 0.5;
					var rad:int = _symbolHeight * 0.25;
					_paylineView.addCircle(x, y, rad, i, j);
				}
			}
		}
		
		private function onLineDrawn(e:SlotEvent):void
		{
			if(++_linesDrawn == _model.playfield.winningLines.length) {
				_model.controller.onPaylinesDrawn();
			}
		}
	}
}