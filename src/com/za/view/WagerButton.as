package com.za.view
{
	import flash.geom.Rectangle;
	
	import starling.display.Button;
	import starling.textures.Texture;
	
	public class WagerButton extends Button
	{
		public function WagerButton()
		{
			var up:Texture = Texture.fromTexture(R.getTexture(R.WAGER_BTNS_TX), new Rectangle(0, 0, 41, 46));
			var over:Texture = Texture.fromTexture(R.getTexture(R.WAGER_BTNS_TX), new Rectangle(43, 0, 41, 46));
			var disabled:Texture = Texture.fromTexture(R.getTexture(R.WAGER_BTNS_TX), new Rectangle(86, 0, 41, 46));
			
			super(up, "", up, over, disabled);
		}
	}
}