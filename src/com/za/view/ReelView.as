package com.za.view
{
	import com.za.model.ModelLocator;
	import com.za.model.Reel;
	
	import flash.utils.getTimer;
	
	import starling.display.Sprite;
	import starling.events.Event;

	/**
	 * Represents the collection of symbols in a single Reel
	 */
	public class ReelView extends Sprite
	{
		private static var REELS_ANIMATED:int; // static variable to count the reels when their animations are complete
		
		private var _model:ModelLocator;
		
		private var _id:int;
		private var _reel:Reel;
		
		private var _symbolWidth:int;
		private var _symbolHeight:int;
		
		private var _symbols:Vector.<SymbolView>;
		
		private var _startTime:int;
		private var _scrollTime:int;
		private var _velocity:Number;
		private var _checkFinalPosition:Boolean;
		
		public function ReelView(id:int, symbolWidth:int, symbolHeight:int)
		{
			_model = ModelLocator.instance;
			
			_id = id;
			_reel = _model.playfield.reels[_id];
			
			_symbolWidth = symbolWidth;
			_symbolHeight = symbolHeight;
			
			_symbols = new Vector.<SymbolView>();
			createSymbols();
		}
		
		/**
		 * Instantiate and add the SymbolViews to this display list.
		 * Rather than instantiate one display object for each symbol,
		 * create only one more than the field of view displays when not spinning.
		 * Symbols are recycled to the bottom of the display when they scroll out of view
		 */
		private function createSymbols():void
		{
			for(var i:uint=0;i < _model.playfield.rows + 1;++i)
			{
				_symbols.push(new SymbolView(_id, i, _symbolHeight));
				_symbols[i].y = _symbolHeight * i;
				addChild(_symbols[i]);
			}
		}
		
		/**
		 * Begin the scrolling animation for this reel.
		 * The reels spin for a random amount of time (3 seconds + up to 5 seconds)
		 * with a random start velocity (10 + up to 5px)
		 */
		public function animateSpin():void
		{
			REELS_ANIMATED = 0;
						
			for each(var symbol:SymbolView in _symbols) {
				symbol.updateReelPosition(_reel.position);
			}
						
			_startTime = getTimer();
			_scrollTime = 3000 + 5000 * Math.random();
			_velocity = 10 + Math.random() * 5;
			_checkFinalPosition = false;
			
			addEventListener(Event.ENTER_FRAME, updatePosition);
		}
		
		/**
		 * Updates the position of the symbols in the display list.
		 * Symbols are placed at the bottom of the display when they scroll past the top.
		 */
		private function updatePosition():void
		{
			// Check the amount of time spent scrolling so far. If it's surpassed the random amount, start slowing the velocity
			if(getTimer() > _startTime + _scrollTime)
			{
				if(_velocity > 3)	// drag the velocity if it hasn't reached the terminal amount

				{
					_velocity *= 0.99;
				}
				else if(_velocity <= 3) // it has reached the terminal amount, check for the correct symbol to stop on
				{
					_velocity = 3;
					_checkFinalPosition = true;
				}
			}
			
			for(var i:uint=0;i < _symbols.length;++i) {
				var symbol:SymbolView = _symbols[i];
				
				//	The symbol with offset == 0 is the "top-most" symbol in this reel. If the terminal velocity about to applied will take it past the edge, stop scrolling
				// 	Only stop if the top edge hasn't scrolled past yet (symbol.y > 0), as it could cause a sudden jump if applied while the symbol is already well past the top
				if(_checkFinalPosition && symbol.offset == 0 && symbol.y <= _velocity && symbol.y > 0)
				{
					stopScrollingAt(i);
					removeEventListener(Event.ENTER_FRAME, updatePosition);
					continue;
				}
				
				symbol.y -= _velocity;
				
				if(symbol.y < -_symbolHeight) {
					symbol.offset += _symbols.length;
					symbol.y += (_model.playfield.rows + 1) * _symbolHeight;
				}
			}
		}
		
		/**
		 * The animation has stopped running, move all the symbols to their correct stopping points
		 */
		private function stopScrollingAt(index:int):void
		{
			for(var i:int=index;i < (index + _model.playfield.rows) % _symbols.length;++i) {
				_symbols[i].y = i * _symbolHeight;
			}				
				
			// Increment REELS_ANIMATED so that we know when all of the reels have stopped spinning and can advance the game
			if(++REELS_ANIMATED == _model.playfield.cols) {
				_model.controller.handleSpinsEnded();
			}
		}
	}
}