package com.za.view
{
	import com.za.model.ModelLocator;
	import com.za.model.Reel;
	
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	/**
	 * Represents the individual symbols displayed in the reels
	 */
	public class SymbolView extends Sprite
	{
		private var _model:ModelLocator;
		private var _reelId:int;
		private var _reelPosition:int;			// reelPosition is the index of the top-most symbol in the current reel
		private var _offset:int;				// offset is this symbols index according to it's distance from the reelPosition. Used to update the texture when the symbol is recycled during animation
		private var _symbolHeight:Number;
		
		private var _image:Image;
		
		public function SymbolView(reelId:int, offset:int, height:int)
		{			
			_model = ModelLocator.instance;
			
			_reelId = reelId;
			_reelPosition = _model.playfield.reels[_reelId].position;
			_offset = offset;
			_symbolHeight = height;
			draw();
		}
		
		/**
		 * Draw the symbol by copyong the proper texture from the symbols sprite sheet.
		 */
		private function draw():void
		{
			var reel:Reel = _model.playfield.reels[_reelId];
			var symIndex:int = reel.getSymbolAt(_offset);
						
			var info:Object = _model.symbolInfo[symIndex];
			var rect:Rectangle = new Rectangle(info.x, info.y, info.width, info.height);
			var texture:Texture = Texture.fromTexture(R.getTexture(R.SPRITE_SHEET_TX), rect, null, false, info.height/_symbolHeight);
		
			if(!_image) {
				_image = new Image(texture);
				addChild(_image);
			}
			else {
				_image.texture = texture;
			}
		}
		
		/**
		 * Update the offset so that it matches the new reelPosition after randomizing a reel.
		 * Skipping this step would result in all the symbols changing instantly when the reels were spun
		 */
		public function updateReelPosition(newPosition:int):void {
						
			var diff:int = _reelPosition - newPosition;
			_reelPosition = newPosition;			
			offset += diff;

		}
		
		public function get offset():int {
			return _offset;
		}
		
		public function set offset(value:int):void {
			if(value < 0)
				_offset = _model.numSymbols + value;
			else if(value >= _model.numSymbols)
				_offset = value % _model.numSymbols;
			else
				_offset = value;
			
			draw();
		}
		
		
	}
}