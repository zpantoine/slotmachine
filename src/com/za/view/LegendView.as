package com.za.view
{
	import com.za.model.ModelLocator;
	
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.textures.Texture;
	
	/**
	 * The legend showing the bonus multipliers for all of the symbols
	 */
	public class LegendView extends Sprite
	{
		private static const COLS:int = 5; // Hard-coded value based on the max number of symbols (10).
		private static const XPADDING:int = 30;
		private static const YPADDING:int = 90;
		
		private static const TITLE_FMT:TextFormat = new TextFormat("sans", 28, 0xffffff);
		private static const LABEL_FMT:TextFormat = new TextFormat("sans", 20, 0xffffff);
		
		private var _model:ModelLocator;
		
		private var _displayWidth:int;
		private var _displayHeight:int;
		
		private var _block:Image;		// receive touch input for the entire screen, not just for the visible background
		private var _background:Image;
		
		private var _title:TextField;
		
		public function LegendView(width:int, height:int)
		{
			_model = ModelLocator.instance;
			
			_displayWidth = width;
			_displayHeight = height;
			
			_block = new Image(Texture.fromColor(_model.controller.stage.stageWidth, _model.controller.stage.stageHeight, 0, 0));
			addChild(_block);
			
			_background = new Image(Texture.fromColor(_displayWidth, _displayHeight, 0x0, 0.9));
			_background.x = (_model.controller.stage.stageWidth - _displayWidth) * 0.5;
			_background.y = (_model.controller.stage.stageHeight - _displayHeight) * 0.5;
			addChild(_background);
			
			_title = new TextField(_background.width, 100, "Bonus Legend", TITLE_FMT);
			_title.x = _background.x;
			_title.y = _background.y;
			addChild(_title);
			
			createLegend();
		}
		
		/** 
		 * Display patterns in a grid format
		 */
		private function createLegend():void
		{
			var startx:int = _background.x + XPADDING;
			var starty:int = _title.y + _title.height + YPADDING;
			
			var gaps:int = COLS + 1;
			var symbolWidth:int = (_displayWidth - gaps * XPADDING) / COLS;
			for(var i:uint=0;i < _model.numSymbols;++i) {
				
				var info:Object = _model.symbolInfo[i];
				var image:Image = new Image(Texture.fromTexture(R.getTexture(R.SPRITE_SHEET_TX), new Rectangle(info.x, info.y, info.width, info.height), null, false, info.width / symbolWidth));
				image.x = startx + (i % COLS) * (symbolWidth + XPADDING);
				image.y = starty + (image.height + YPADDING) * int(i / COLS);
				addChild(image);
				
				var label:TextField = new TextField(symbolWidth, 40, info.multiplier + "%", LABEL_FMT);
				label.x = image.x;
				label.y = image.y - label.height;
				addChild(label);
			}
		}
	}
}