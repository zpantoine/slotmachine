package com.za.view
{
	import com.za.model.ModelLocator;
	
	import flash.globalization.CurrencyFormatter;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.textures.Texture;
	
	/**
	 * Manages all UI and display elements
	 */
	public class MainView extends Sprite
	{
		private static const UI_TEXT_FORMAT:TextFormat = new TextFormat("sans", 32, 0xd292a3);
		private static const MULTI_TEXT_FORMAT:TextFormat = new TextFormat("sans", 64, 0xd292a3);
		private static const LOADING_FORMAT:TextFormat = new TextFormat("_sans", 45, 0xffffff);
		
		private static const currencyFormatter:CurrencyFormatter = new CurrencyFormatter("en-US");
		
		private var _model:ModelLocator;
		
		private var background:Image;
		private var playfieldView:PlayfieldView;
		private var playscreen:Image;
		private var spinBtn:Button;
		private var loadingScreen:Image;		
		private var loadingMessage:TextField;
		
		private var balanceTF:TextField;
		private var wagerTF:TextField;
		private var increaseWagerBtn:Button;
		private var decreaseWagerBtn:Button;
		
		private var multiplierTF:TextField;
		private var multiplierFrame:Image;
		
		private var infoBtn:Button;
		private var legend:LegendView;
		
		public function MainView()
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void
		{
			_model = ModelLocator.instance;
						
			background = new Image(R.getTexture(R.BACKGROUND_TX));
			addChild(background);
			
			balanceTF = new TextField(275, 60, "", UI_TEXT_FORMAT);
			balanceTF.wordWrap = false;
			balanceTF.x = 155;
			balanceTF.y = 620;
			addChild(balanceTF);
			
			wagerTF = new TextField(235, 60, "", UI_TEXT_FORMAT);
			wagerTF.wordWrap = false;
			wagerTF.x = 865;
			wagerTF.y = 620;
			addChild(wagerTF);
			
			increaseWagerBtn = new WagerButton();
			increaseWagerBtn.x = 1105;
			increaseWagerBtn.y = 600;
			addChild(increaseWagerBtn);
			
			decreaseWagerBtn = new WagerButton();
			decreaseWagerBtn.scaleY = -1;
			decreaseWagerBtn.x = 1105;
			decreaseWagerBtn.y = 700;
			addChild(decreaseWagerBtn);
						
			playscreen = new Image(R.getTexture(R.SCREEN_OVERLAY_TX));
			playscreen.x = 135;
			playscreen.y = 85;
			addChild(playscreen);
			
			playfieldView = new PlayfieldView(895, 365);
			playfieldView.x = playscreen.x + 43;
			playfieldView.y = playscreen.y + 37;
			addChildAt(playfieldView, 1);
			
			spinBtn = new Button(R.getTexture(R.SPIN_BTN_UP_TX), "", R.getTexture(R.SPIN_BTN_DOWN_TX), R.getTexture(R.SPIN_BTN_UP_TX), R.getTexture(R.SPIN_BTN_DIS_TX));
			spinBtn.x = 460;
			spinBtn.y = 580;
			spinBtn.enabled = false;
			addChild(spinBtn);
			
			loadingScreen = new Image(R.getTexture(R.LOADING_SCREEN_TX));
			addChild(loadingScreen);
			
			loadingMessage = new TextField(1280, 200, "Loading...", LOADING_FORMAT);
			loadingMessage.y = 500;
			addChild(loadingMessage);
			
			multiplierTF = new TextField(1280, 600, "", MULTI_TEXT_FORMAT);
			multiplierFrame = new Image(Texture.fromColor(1280, 210, 0x0, 0.9));
			multiplierFrame.y = 200;
			
			infoBtn = new Button(Texture.fromColor(100, 40, 0x0, 0.6), "info");
			infoBtn.textFormat = UI_TEXT_FORMAT;

			infoBtn.x = _model.controller.stage.stageWidth - 110;
			infoBtn.y = _model.controller.stage.stageHeight - 50;
			infoBtn.addEventListener(TouchEvent.TOUCH, onInfoTouch);
			addChild(infoBtn);
		}
		
		public function loadComplete():void
		{			
			loadingMessage.text = "Click Anywhere to Start!";
			loadingScreen.addEventListener(TouchEvent.TOUCH, onStartTouch);
			loadingMessage.addEventListener(TouchEvent.TOUCH, onStartTouch);
			
			playfieldView.createReels();
		}
		
		private function onStartTouch(e:TouchEvent):void {
			if(e.touches[0].phase == "ended") {
				
				enableTouches();
				
				loadingScreen.removeEventListener(TouchEvent.TOUCH, onStartTouch);
				loadingMessage.removeEventListener(TouchEvent.TOUCH, onStartTouch);

				if(this.contains(loadingScreen))
					this.removeChild(loadingScreen);
		
				if(this.contains(loadingMessage))
					this.removeChild(loadingMessage);
			}
		}
		
		public function animateReels():void
		{
			playfieldView.animateReels();
		}
		
		public function drawPaylines():void
		{
			playfieldView.drawPaylines();
		}
		
		public function showMultiplier(value:int):void
		{
			var multiText:Tween = new Tween(multiplierTF, 0.7);
			multiText.animate("alpha", 1);
			multiText.onStart = function():void {
				var percentIncrease:Number = (100 + value) * 0.01;
				multiplierTF.text = value + "% Multiplier Bonus!\n$" + int(_model.player.wager * percentIncrease) + " won!";
				multiplierTF.alpha = 0;
				addChild(multiplierTF);
			};
				
			multiText.onComplete = function():void {
				hideMultiplier(value);
			};
				
			var frameTween:Tween = new Tween(multiplierFrame, 0.7);
			frameTween.animate("alpha", 0.9);
			frameTween.onStart = function():void {
				multiplierFrame.alpha = 0;
				addChild(multiplierFrame);
			};
				
			Starling.juggler.add(frameTween);
			Starling.juggler.add(multiText);
		}
		
		private function hideMultiplier(value:int):void
		{
			var multiText:Tween = new Tween(multiplierTF, 0.7);
			multiText.delay = 2;
			multiText.animate("alpha", 0);			
			multiText.onComplete = function():void {
				if(contains(multiplierTF)) {
					removeChild(multiplierTF);
				}
				
				_model.controller.onMultiplierShown(value);
			};
			
			var frameTween:Tween = new Tween(multiplierFrame, 0.7);
			frameTween.delay = 2;
			frameTween.animate("alpha", 0);
			frameTween.onComplete = function():void {
				if(contains(multiplierFrame)) {
					removeChild(multiplierFrame);
				}
			};
			
			Starling.juggler.add(frameTween);
			Starling.juggler.add(multiText);
		}
		
		/**
		 * Activate buttons
		 */
		public function enableTouches():void
		{
			spinBtn.enabled = true;
			spinBtn.addEventListener(TouchEvent.TOUCH, onSpinTouch);
			
			increaseWagerBtn.enabled = true;
			increaseWagerBtn.addEventListener(TouchEvent.TOUCH, onIncreaseWager);
			
			decreaseWagerBtn.enabled = true;
			decreaseWagerBtn.addEventListener(TouchEvent.TOUCH, onDecreaseWager);
		}
		
		/**
		 * Disable buttons to prevent unwanted interaction
		 */
		public function disableTouches():void
		{
			spinBtn.enabled = false;
			spinBtn.removeEventListener(TouchEvent.TOUCH, onSpinTouch);
			
			increaseWagerBtn.enabled = false;
			increaseWagerBtn.removeEventListener(TouchEvent.TOUCH, onIncreaseWager);
			
			decreaseWagerBtn.enabled = false;
			decreaseWagerBtn.removeEventListener(TouchEvent.TOUCH, onDecreaseWager);
		}
		
		/**
		 * 	Update the display with player fund info 
		 */
		public function updatePlayerFields():void
		{			
			if(!_model.player)
				return;
					
			balanceTF.text = currencyFormatter.format(_model.player.balance, true);
			wagerTF.text = currencyFormatter.format(_model.player.wager, true);
		}
		
		/**
		 * Display message to players that have lost all of their funds.
		 */
		public function showEndScreen():void
		{
			addChild(loadingScreen);
			addChild(loadingMessage);
			
			loadingMessage.text = "You lost all of your money!\nClick anywhere to reload your balance and try again."
				
			loadingScreen.addEventListener(TouchEvent.TOUCH, onRestartTouch);
			loadingMessage.addEventListener(TouchEvent.TOUCH, onRestartTouch);
		}
		
		public function showLegend():void
		{
			if(!legend) {
				legend = new LegendView(800, 600);
			}
			
			addChild(legend);
			
			var lTween:Tween = new Tween(legend, 0.5);
			lTween.onStart = function():void {
				legend.y = -legend.height;
			};
			lTween.onComplete = function():void {
				legend.addEventListener(TouchEvent.TOUCH, onLegendTouch);
			}
			lTween.animate("y", 0);
			Starling.juggler.add(lTween);
		}
		
		public function hideLegend():void
		{
			legend.removeEventListener(TouchEvent.TOUCH, onLegendTouch);

			var lTween:Tween = new Tween(legend, 0.5);
			lTween.onComplete = function():void {
				if(contains(legend)) {
					removeChild(legend);
				}
			}
			lTween.animate("y", -legend.height);
			Starling.juggler.add(lTween);
		}
		
		private function onRestartTouch(e:TouchEvent):void {
			if(e.touches[0].phase == "ended") {
				
				enableTouches();
				_model.controller.startGame();
				
				loadingScreen.removeEventListener(TouchEvent.TOUCH, onRestartTouch);
				loadingMessage.removeEventListener(TouchEvent.TOUCH, onRestartTouch);
				
				if(this.contains(loadingScreen))
					this.removeChild(loadingScreen);
				
				if(this.contains(loadingMessage))
					this.removeChild(loadingMessage);
			}
		}
		
		// Handlers for button presses
		
		private function onSpinTouch(e:TouchEvent):void
		{
			if(e.touches[0] && e.touches[0].phase == "ended") 
			{
				_model.controller.handleSpinPress();
			}
		}
		
		private function onIncreaseWager(e:TouchEvent):void
		{
			if(e.touches[0] && e.touches[0].phase == "ended") 
			{
				_model.controller.handleIncreaseWager();
			}
		}
		
		private function onDecreaseWager(e:TouchEvent):void
		{
			if(e.touches[0] && e.touches[0].phase == "ended") 
			{
				_model.controller.handleDecreaseWager();
			}
		}
		
		private function onInfoTouch(e:TouchEvent):void
		{
			if(e.touches[0].phase == "ended") {
				_model.controller.handleInfoTouch();
			}
		}
		
		private function onLegendTouch(e:TouchEvent):void
		{
			if(e.touches[0].phase == "ended") {
				_model.controller.handleLegendTouch();
			}
		}
	}
}