package com.za.events
{
	import starling.events.Event;
	
	/**
	 * Custom events class
	 */
	public class SlotEvent extends Event
	{		
		public static const LOAD_COMPLETE:String = "LoadComplete";
		public static const LOAD_ERROR:String = "LoadError";
		public static const PAYLINE_DRAWN:String = "PaylineDrawn";
		
		public function SlotEvent(type:String, bubbles:Boolean=false, data:Object=null)
		{
			super(type, bubbles, data);
		}
	}
}