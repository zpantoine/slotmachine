package
{
	import com.za.events.SlotEvent;
	import com.za.model.ModelLocator;
	import com.za.utils.DataLoader;
	import com.za.view.MainView;
	
	import flash.display.Bitmap;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class GameController extends Sprite
	{
		/**
		 * Main Controller class for the game
		 */
		
		private var _model:ModelLocator;
		private var _view:MainView;
		
		public function GameController()
		{
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		/**
		 * Instantiate the model and view and begin config loading process
		 */
		private function initialize(e:Event):void
		{
			_model = ModelLocator.instance;
			_model.controller = this;
			
			_view = new MainView();
			addChild(_view);
			
			loadConfig();
		}
		
		/**
		 * Load external json file that will populate the model
		 */
		private function loadConfig():void
		{
			var loader:DataLoader = new DataLoader();
			loader.addEventListener(SlotEvent.LOAD_COMPLETE, onLoadComplete);
			loader.addEventListener(SlotEvent.LOAD_ERROR, onLoadError);
			loader.load("data/config.json", DataLoader.TYPE_JSON);
		}
		
		private function onLoadComplete(e:SlotEvent):void
		{
			var data:String = e.data as String;
			_model.config = JSON.parse(data);
		}
		
		private function onLoadError(e:SlotEvent):void
		{
			trace("error loading external data");
		}
		
		/**
		 * Load the external sprite sheet containing the symbols
		 */
		public function loadExternalImages():void
		{
			var loader:DataLoader = new DataLoader();
			loader.addEventListener(SlotEvent.LOAD_COMPLETE, onSpritesLoadComplete);
			loader.addEventListener(SlotEvent.LOAD_ERROR, onSpritesLoadError);
			loader.load(_model.spritesheetURL, DataLoader.TYPE_IMAGE);
		}
		
		private function onSpritesLoadComplete(e:SlotEvent):void
		{
			var sprites:Bitmap = e.data as Bitmap;
			R.saveTexture(R.SPRITE_SHEET_TX, sprites);
			
			_view.loadComplete();

			startGame();
		}
		
		private function onSpritesLoadError(e:SlotEvent):void
		{
			trace("Error loading in external images");
		}
		
		/**
		 * Begin a new game. Reset the player to the starting balance amount and randomize the position of the reels
		 */
		public function startGame():void
		{			
			_model.player.reset();
			_model.playfield.spinReels();
		}
		
		/**
		 * Player has depleted their funds, end the game
		 */
		private function endGame():void
		{
			_view.disableTouches();
			_view.showEndScreen();
		}
		
		/**
		 * The player has spun the reels and submitted a wager
		 */
		public function handleSpinPress():void
		{
			_view.disableTouches();
			_model.playfield.spinReels();
			_view.animateReels();
						
			this.touchable = false;
		}
		
		/**
		 * Check the reels for a winning situation, otherwise deplete the balance
		 */
		public function handleSpinsEnded():void
		{			
			_model.playfield.checkPaylines();
			
			if(_model.playfield.winningLines.length > 0) {
				_view.drawPaylines();	
			}
			else {				
				this.touchable = true;
				_view.enableTouches();
				
				_model.player.deductWager();
				
				if(_model.player.balance == 0) {
					
					endGame();
				}
			}
		}
		
		/**
		 * The paylines animation has finished, show the user their multiplier bonus
		 */
		public function onPaylinesDrawn():void
		{
			var multi:int;
			
			for(var i:uint=0;i < _model.playfield.winningLines.length;++i) {
				
				var index:int = _model.playfield.winningLines[i][0];
				var symbol:int = _model.playfield.reels[0].getSymbolAt(index);
				var multiplier:int = _model.symbolInfo[symbol].multiplier;
				
				multi += multiplier;
			}
			
			_view.showMultiplier(multi);
		}
		
		/**
		 * The multiplier dialog has closed, activate the UI to accept input
		 */
		public function onMultiplierShown(multiplier:int):void
		{
				this.touchable = true;
				_view.enableTouches();
				
				_model.player.addWinnings(multiplier);
		}
		
		// Convenience function for user interactions.
		
		public function handleIncreaseWager():void
		{
			_model.player.increaseWager();
		}
		
		public function handleDecreaseWager():void
		{
			_model.player.decreaseWager();
		}
		
		public function handlePlayerUpdate():void
		{
			_view.updatePlayerFields();
		}
		
		public function handleInfoTouch():void
		{
			_view.disableTouches();
			_view.showLegend();
		}
		
		public function handleLegendTouch():void
		{
			_view.enableTouches();
			_view.hideLegend();
		}
	}	
}